from django.shortcuts import render,redirect
from django.http import JsonResponse
import json
import requests
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib import messages
from django.urls import reverse
from django.contrib.auth import logout as django_logout


# Create your views here.
def index(request):
    if request.method == 'GET':
        return render(request, 'index.html')
    else:

        username = request.POST.get("username")
        password = request.POST.get("password")
        # fullname = request.POST.get("fullname")
        user = authenticate(username = username, password = password)
        if(user is not None):
            login(request,user)
            return redirect('hologin:afterlogin')

        messages.error(request,"Wrong password or username!!")
        return render(request, 'index.html')

def signup(request):

    if(request.method == 'GET'):
        return render(request, 'signup.html')
        
    else:
        username = request.POST.get("username")
        password = request.POST.get("password")
        # fullname = request.POST.get("fullname")
        try:
            user = User.objects.get(username = username)
            messages.error(request,"Username taken, try again!!")
            return render(request,'signup.html')
        except:
            user = User.objects.create_user(username = username, password = password)
            messages.success(request,"Sign up successfull!")

            
            return redirect(reverse('hologin:index'))


@login_required(login_url = 'hologin:index')
def afterlogin(request):
    if request.method == 'GET':
        return render(request,'afterlogin.html')

def logout(request):
    django_logout(request)
    return redirect('hologin:index')
