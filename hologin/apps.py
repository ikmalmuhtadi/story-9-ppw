from django.apps import AppConfig


class HologinConfig(AppConfig):
    name = 'hologin'
