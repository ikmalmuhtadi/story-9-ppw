from django.test import TestCase,Client
from django.urls import resolve
from .views import *
from django.test import LiveServerTestCase


# Create your tests here.
class unitTest_Story9(TestCase):
	def test_app_url_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code,200)
	def test_view_def_exist(self):
		found = resolve('/')
		self.assertEqual(found.func, index)
	def test_view_def_exist2(self):
		found = resolve('/signup')
		self.assertEqual(found.func, signup)
	def test_app_url_exist2(self):
		response = Client().get('/signup')
		self.assertEqual(response.status_code,200)

