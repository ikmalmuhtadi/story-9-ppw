from django.urls import path
from . import views

app_name = 'hologin'

urlpatterns = [
    path('', views.index, name='index'),
    path('welcome', views.afterlogin, name='afterlogin'),
    path('signup', views.signup, name='signup'),
    path('logout',views.logout,name='logout'),
]